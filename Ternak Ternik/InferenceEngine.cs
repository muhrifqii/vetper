﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ternak_Ternik
{
    class InferenceEngine
    {
        public const double CF_TREESHOLD = 0.65;
        private List<InferenceModel> inference;
        private InferenceModel tempInferenceModel;
        private List<Rule> rules;
        private Dictionary<string, Item> items;
        private Dictionary<string, Item> haveBeenShownItem;

        //public Dictionary<string, Item> HaveBeenShownItem
        //{
        //    get { return haveBeenShownItem; }
        //}

        /// <summary>
        /// This is the core of expert system
        /// </summary>
        /// <param name="rules">rule</param>
        /// <param name="items">all evidences that are going to be changed</param>
        public InferenceEngine(List<Rule> rules, Dictionary<string, Item> items)
        {
            this.rules = rules;
            this.items = items;
            haveBeenShownItem = new Dictionary<string, Item>();
            ResetInferenceEngine();
            ResetTemporary();
        }
        public void ResetInferenceEngine()
        {
            inference = new List<InferenceModel>();
        }
        private void ResetTemporary()
        {
            tempInferenceModel = new InferenceModel();
        }
        private void ResetCF()
        {
            //nilai item berubah rapopolah, bingung mergo pass by reference, jadi ada banyak sekali reference memory address di register :v
            foreach (var item in items)
            {
                item.Value.CF = 0;
            }
        }
        public void AddWorkingMemory(Item workingMemory)
        {
            //inference.WorkingMemory.Add(workingMemory.Key);
            //items[workingMemory.Key] = workingMemory;
            haveBeenShownItem.Add(workingMemory.Key, workingMemory);
        }

        public void Run()
        {
            int iteration = 0;
            ResetCF();
            foreach (var item in haveBeenShownItem)
            {
                tempInferenceModel.WorkingMemory.Add(new WorkingMemoryItem(item.Value, false));
                items[item.Key].CF = item.Value.CF;
                //System.Diagnostics.Debug.WriteLine(item.Value.CF);
                //System.Windows.MessageBox.Show(item.Value.Key + ": " + item.Value.CF.ToString());
            }
            Item x; //stand for then
            foreach (var rule in rules)
            {
                //System.Windows.MessageBox.Show(rule.Key + ", " + rule.AND.Count.ToString() + ", " + rule.OR.Count.ToString() + ", " + rule.THEN);
                if (rule.AND.Count == 1 && rule.OR.Count == 0)
                {
                    x = new Item();
                    x.Key = rule.THEN;
                    x.Description = items[rule.THEN].Description;
                    x.CF = CFHe(items[rule.AND[0]].CF, rule.CF);
                    if (tempInferenceModel.HasEvidence(rule.THEN))
                    {
                        double cfcom = CFCombine(items[rule.THEN].CF, x.CF);
                        x.CF = cfcom;
                        foreach (var wmToBeChanged in tempInferenceModel.WorkingMemory)
                        {
                            if (wmToBeChanged.Item.Key.Equals(x.Key))
                            {
                                wmToBeChanged.Item.CF = x.CF;
                            }
                        }
                    }
                    else tempInferenceModel.WorkingMemory.Add(new WorkingMemoryItem(x, false));
                }
                else if (rule.AND.Count != 0 && rule.OR.Count != 0)
                {
                    //our rules only have one condition for this,
                    //so only for OR inside AND, next work we will handle OR inside AND and vice versa
                    List<Item> li = new List<Item>();
                    foreach (var key in rule.OR)
                    {
                        li.Add(items[key]);
                    }
                    x = Or(li);             // x is the union item
                    li.Clear();             // clear the temporary list to calc the AND operation
                    li.Add(x);              
                    foreach (var key in rule.AND)
                    {
                        li.Add(items[key]);
                    }
                    x = And(li);
                    double tmpp = x.CF;                                     //after OR-AND operation
                    x.CF = CFHe(tmpp, rule.CF);
                    x.Key = items[rule.THEN].Key;
                    x.Description = items[rule.THEN].Description;
                    x.Question = items[rule.THEN].Question;
                    if (!tempInferenceModel.HasEvidence(rule.THEN)) tempInferenceModel.WorkingMemory.Add(new WorkingMemoryItem(x, false));
                    else
                    {
                        double cfcom = CFCombine(items[rule.THEN].CF, x.CF);
                        x.CF = cfcom;
                        foreach (var wmToBeChanged in tempInferenceModel.WorkingMemory)
                        {
                            if (wmToBeChanged.Item.Key.Equals(x.Key))
                            {
                                wmToBeChanged.Item.CF = x.CF;
                            }
                        }
                    }
                }
                else if (rule.AND.Count > 1)
                {
                    List<Item> li = new List<Item>();
                    foreach (var key in rule.AND)
                    {
                        li.Add(items[key]);
                    }
                    x = And(li); // x is the intersection item
                    double tmpp = x.CF;     //cf intersection after AND operator is placed to temporary variable
                    x.CF = CFHe(tmpp, rule.CF);
                    x.Key = items[rule.THEN].Key;
                    x.Description = items[rule.THEN].Description;
                    x.Question = items[rule.THEN].Question;

                    if (!tempInferenceModel.HasEvidence(rule.THEN)) tempInferenceModel.WorkingMemory.Add(new WorkingMemoryItem(x, false));
                    else
                    {
                        double cfcom = CFCombine(items[rule.THEN].CF, x.CF);
                        x.CF = cfcom;
                        foreach (var wmToBeChanged in tempInferenceModel.WorkingMemory)
                        {
                            if (wmToBeChanged.Item.Key.Equals(x.Key))
                            {
                                wmToBeChanged.Item.CF = x.CF;
                            }
                        }
                    }
                }
                else if (rule.OR.Count > 0)
                {
                    List<Item> li = new List<Item>();
                    foreach (var key in rule.OR)
                    {
                        li.Add(items[key]);
                    }
                    x = Or(li);
                    double tmpp = x.CF;     //cf intersection after OR operator
                    x.CF = CFHe(tmpp, rule.CF);
                    x.Key = items[rule.THEN].Key;
                    x.Description = items[rule.THEN].Description;
                    x.Question = items[rule.THEN].Question;
                    if (!tempInferenceModel.HasEvidence(rule.THEN)) tempInferenceModel.WorkingMemory.Add(new WorkingMemoryItem(x, false));
                    else
                    {
                        double cfcom = CFCombine(items[rule.THEN].CF, x.CF);
                        x.CF = cfcom;
                        foreach (var wmToBeChanged in tempInferenceModel.WorkingMemory)
                        {
                            if (wmToBeChanged.Item.Key.Equals(x.Key))
                            {
                                wmToBeChanged.Item.CF = x.CF;
                            }
                        }
                    }
                }
                else
                {
                    x = new Item();
                    x.CF = -100;
                    System.Windows.MessageBox.Show("Something went wrong when running inference");
                }
                
                items[rule.THEN].CF = x.CF;                                     //set the new CF to the dictionarylist of items
                tempInferenceModel.Iteration = iteration++;                     //iteration for every rule fired
                tempInferenceModel.RuleFired = rule.Key;                        //save the fired rule
                inference.Add(tempInferenceModel);                              //go to the next iteration
                InferenceModel tmp = new InferenceModel();
                tmp.WorkingMemory.AddRange(tempInferenceModel.WorkingMemory);   //
                //foreach (var itt in tempInferenceModel.WorkingMemory)
                //{
                //    System.Diagnostics.Debug.WriteLine(itt.Item.Key + ", " + itt.Item.CF);
                //}
                //System.Diagnostics.Debug.WriteLine("endl");
                ResetTemporary();                                               //set the next iteration with the current working memory
                tempInferenceModel.WorkingMemory.AddRange(tmp.WorkingMemory);   //
            }
            ResetTemporary();
        }

        public bool IsHere(string name)
        {
            foreach (var item in haveBeenShownItem)
            {
                if(item.Key.Equals(name) && item.Value.Key.Equals(name)) return true;
            }
            return false;
        }

        public string TheGreatestOfAll()
        {
            Item max = new Item()
            {
                CF = 0
            };
            List<Item> li = new List<Item>();
            foreach (var item in items)
            {
                if (item.Key.Contains("Z"))
                {
                    if (item.Value.CF >= max.CF)
                    {
                        max.CF = item.Value.CF;
                        max.Key = item.Value.Key;
                    }
                }
            }
            return max.Key;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="CFEe">CF Evidence</param>
        /// <param name="CFHE">CF Rule</param>
        /// <returns></returns>
        private double CFHe(double CFEe, double CFHE)
        {
            return CFEe * CFHE;
        }

        private double CFCombine(double CF1, double CF2)
        {
            double hitung = 0;
            if (CF1 >= 0 && CF2 >= 0)
            {
                hitung = CF1 + CF2 * (1 - CF1);
            }
            else if (CF1 < 0 && CF2 < 0)
            {
                hitung = CF1 + CF2 * (1 + CF1);
            }
            else
            {
                hitung = (CF1 + CF2) / (1 - Math.Min(Math.Abs(CF1), Math.Abs(CF2)));
            }
            return hitung;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="vals">List of Item in AND</param>
        /// <returns>new Item from AND</returns>
        private Item And(List<Item> vals)
        {
            Item x = new Item();
            x.CF = 100;
            for (int i = 0; i < vals.Count; i++)
            {
                if (vals[i].CF < x.CF)
                {
                    x.Key = vals[i].Key;
                    x.CF = vals[i].CF;
                    x.Description = vals[i].Description;
                    x.Question = vals[i].Question;
                }
            }
            return x;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="vals">List of Item in OR</param>
        /// <returns>new Item from OR</returns>
        private Item Or(List<Item> vals)
        {
            Item x = new Item();
            x.CF = -100;
            for (int i = 0; i < vals.Count; i++)
            {
                if (vals[i].CF > x.CF)
                {
                    x.Key = vals[i].Key;
                    x.CF = vals[i].CF;
                    x.Description = vals[i].Description;
                    x.Question = vals[i].Question;
                }
            }
            return x;
        }

        private class WorkingMemoryItem
        {
            public Item Item { get; set; }
            public bool IsConclusion { get; set; }
            public WorkingMemoryItem(Item wm, bool isConclusion)
            {
                Item = wm;
                IsConclusion = isConclusion;
            }
        }

        private class InferenceModel
        {
            private List<WorkingMemoryItem> wm = new List<WorkingMemoryItem>();
            public List<WorkingMemoryItem> WorkingMemory
            {
                get { return wm; }
                set
                {
                    if (value != wm) wm = value;
                }
            }
            public int Iteration { get; set; }
            public string RuleFired { get; set; }

            public bool HasEvidence(string key)
            {
                foreach (var wom in wm)
                {
                    if (wom.Item.Key.Equals(key)) return true;
                }
                return false;
            }
        }
    }

    
}
