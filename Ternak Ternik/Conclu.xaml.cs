﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using System.IO;
using Newtonsoft.Json;

namespace Ternak_Ternik
{
    public partial class Conclu : PhoneApplicationPage
    {
        private string jsonfile = "Assets/concluc.json";
        private Item output;

        private string name;
        private string menular;
        private string penyebab;
        private string rekomendasi;
        private string cf;

        private List<ConclusionItem> conclusions;

        public Conclu()
        {
            InitializeComponent();
            Content.Loaded += Content_Loaded;
        }

        void Content_Loaded(object sender, RoutedEventArgs e)
        {
            try
            {
                this.nameUI.Text = name;
                this.cfUI.Text = cf;
                this.menularUI.Text = menular;
                this.penyebabUI.Text = penyebab;
                this.rekomendasiUI.Text = rekomendasi;
            }
            catch(Exception ex)
            {
                MessageBox.Show(ex.Message.ToString());
            }
        }

        protected override void OnBackKeyPress(System.ComponentModel.CancelEventArgs e)
        {

            base.OnBackKeyPress(e);
        }

        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            base.OnNavigatedTo(e);
            output = PhoneApplicationService.Current.State["item"] as Item;

            string json;
            using (StreamReader objReader = new StreamReader(jsonfile))
            {
                json = objReader.ReadToEnd();
            }
            conclusions = JsonConvert.DeserializeObject<List<ConclusionItem>>(json);
            ShowConclusion();
        }

        private void ShowConclusion()
        {
            try
            {
                foreach (var item in conclusions)
                {
                    if (this.output.Key.Equals(item.Kode))
                    {
                        name = item.NamaPenyakit;
                        menular = item.Menular;
                        penyebab = item.Penyebab;
                        rekomendasi = item.RekomendasiTindakan;
                        cf = output.CF.ToString();
                    }
                }
            }
            catch
            {
                MessageBox.Show("Oops.. Something went wrong");
            }
           
        }
    }
}