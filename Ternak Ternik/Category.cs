﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ternak_Ternik
{
    class Category
    {
        private List<string> evidences = new List<string>();
        public List<string> Evidences
        {
            get { return evidences; }
            set
            {
                if (value != evidences) evidences = value;
            }
        }

        public Category()
        {
            gen();
            disease.Add("Z001", bef);
            disease.Add("Z002", bvd);
            disease.Add("Z003", bvd2);
            disease.Add("Z004", pmk);
        }

        private List<string> bef = new List<string>();
        private List<string> bvd = new List<string>();
        private List<string> bvd2 = new List<string>();
        private List<string> pmk = new List<string>();
        private Dictionary<string, List<string>> disease = new Dictionary<string, List<string>>();

        public List<string> BEF
        {
            get { return bef; }
        }
        public List<string> BVD
        {
            get { return bvd; }
        }
        public List<string> BVD2
        {
            get { return bvd2; }
        }
        public List<string> PMK
        {
            get { return pmk; }
        }
        public Dictionary<string, List<string>> Disease
        {
            get { return disease; }
        }

        private void gen()
        {
            bef.Add("X001");
            bvd.Add("X001");
            bvd2.Add("X001");
            pmk.Add("X001");

            bef.Add("X002");
            bvd.Add("X002");
            bvd2.Add("X002");
            pmk.Add("X002");

            bef.Add("X003");
            bvd.Add("X003");
            bvd2.Add("X003");
            pmk.Add("X003");

            bef.Add("X004");
            bvd.Add("X004");
            bvd2.Add("X004");
            pmk.Add("X004");

            bef.Add("X005");

            bef.Add("X006");
            pmk.Add("X006");

            bef.Add("X007");
            pmk.Add("X007");

            bef.Add("X008");
            pmk.Add("X008");

            bvd.Add("X009");
            bvd2.Add("X009");

            bvd.Add("X010");
            bvd2.Add("X010");

            bvd.Add("X011");
            bvd2.Add("X011");

            bvd.Add("X012");
            bvd2.Add("X012");

            bvd.Add("X013");
            bvd2.Add("X013");

            bvd.Add("X014");
            bvd2.Add("X014");

            bvd.Add("X015");
            bvd2.Add("X015");

            bvd.Add("X016");
            bvd2.Add("X016");

            bvd.Add("X017");
            bvd2.Add("X017");

            bvd.Add("X018");
            bvd.Add("X019");
            bvd2.Add("X020");
            bvd2.Add("X021");
            bvd2.Add("X022");
            pmk.Add("X023");
            pmk.Add("X024");
            pmk.Add("X025");
            pmk.Add("X026");
            pmk.Add("X027");
            pmk.Add("X028");
            pmk.Add("X029");
            pmk.Add("X030");
            pmk.Add("X031");
            pmk.Add("X032");
            pmk.Add("X033");
            pmk.Add("X034");
            pmk.Add("X035");
            pmk.Add("X036");
        }
    }
}
