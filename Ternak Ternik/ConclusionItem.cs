﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ternak_Ternik
{
    class ConclusionItem
    {
        public int No { get; set; }
        public string Kode { get; set; }
        public string NamaPenyakit { get; set; }
        public string Menular { get; set; }
        public string Penyebab { get; set; }
        public string RekomendasiTindakan { get; set; }
    }
}
