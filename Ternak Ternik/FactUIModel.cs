﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ternak_Ternik
{
    class FactUIModel : INotifyPropertyChanged
    {
        private double val;
        public double Value
        {
            get { return val; }
            set
            {
                if (val == value) { return; }
                val = value;
                NotifyPropertyChanged("Value");
            }
        }
        public string Question { get; set; }
        public string Id { get; set; }
        public bool IsEnabled { get; set; }

        public event PropertyChangedEventHandler PropertyChanged;
        private void NotifyPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = this.PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }

    }
}
