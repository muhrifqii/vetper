﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ternak_Ternik
{
    public class Rule
    {
        private List<string> and = new List<string>();
        private List<string> or = new List<string>();

        public string Key { get; set; }
        public List<string> AND
        {
            get { return and; }
            set
            {
                //if (and == null) and = new List<string>();
                if (and != value) and = value;
            }
        }

        public List<string> OR
        {
            get { return or; }
            set
            {
                //if (or == null) or = new List<string>();
                if (or != value) or = value;
            }
        }
        public string THEN { get; set; }
        public double CF { get; set; }

    }
}
