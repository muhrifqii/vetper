﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using System.Collections.ObjectModel;
using System.Windows.Media;
using System.Reflection;
using Telerik.Windows.Controls;
using System.IO;
using Newtonsoft.Json;

namespace Ternak_Ternik
{
    public partial class MainActivity : PhoneApplicationPage
    {
        //public T FindDescendant<T>(DependencyObject obj) where T : DependencyObject
        //{
        //    // Check if this object is the specified type
        //    if (obj is T)
        //        return obj as T;

        //    // Check for children
        //    int childrenCount = VisualTreeHelper.GetChildrenCount(obj);
        //    if (childrenCount < 1)
        //        return null;

        //    // First check all the children
        //    for (int i = 0; i < childrenCount; i++)
        //    {
        //        DependencyObject child = VisualTreeHelper.GetChild(obj, i);
        //        if (child is T)
        //            return child as T;
        //    }

        //    // Then check the childrens children
        //    for (int i = 0; i < childrenCount; i++)
        //    {
        //        DependencyObject child = FindDescendant<T>(VisualTreeHelper.GetChild(obj, i));
        //        if (child != null && child is T)
        //            return child as T;
        //    }

        //    return null;
        //}

        //public T FindDescendant<T>(DependencyObject obj, string objname) where T : DependencyObject
        //{
        //    string controlneve = "";

        //    Type tyype = obj.GetType();
        //    if (tyype.GetProperty("Name") != null)
        //    {
        //        PropertyInfo prop = tyype.GetProperty("Name");
        //        controlneve = (string) prop.GetValue((object)obj, null);
        //    }
        //    else
        //    {
        //        return null;
        //    }

        //    if (obj is T && objname.ToString().ToLower() == controlneve.ToString().ToLower())
        //    {
        //        return obj as T;
        //    }

        //    // Check for children
        //    int childrenCount = VisualTreeHelper.GetChildrenCount(obj);
        //    if (childrenCount < 1)
        //        return null;

        //    // First check all the children
        //    for (int i = 0; i <= childrenCount - 1; i++)
        //    {
        //        DependencyObject child = VisualTreeHelper.GetChild(obj, i);
        //        if (child is T && objname.ToString().ToLower() == controlneve.ToString().ToLower())
        //        {
        //            return child as T;
        //        }
        //    }

        //    // Then check the childrens children
        //    for (int i = 0; i <= childrenCount - 1; i++)
        //    {
        //        string checkobjname = objname;
        //        DependencyObject child = FindDescendant<T>(VisualTreeHelper.GetChild(obj, i), objname);
        //        if (child != null && child is T && objname.ToString().ToLower() == checkobjname.ToString().ToLower())
        //        {
        //            return child as T;
        //        }
        //    }

        //    return null;
        //}

        //private ObservableCollection<FactUIModel> factItems;

        public static SolidColorBrush GetColorFromHexa(string hexaColor)
        {
            return new SolidColorBrush(
                Color.FromArgb(
                255,
                    Convert.ToByte(hexaColor.Substring(1, 2), 16),
                    Convert.ToByte(hexaColor.Substring(3, 2), 16),
                    Convert.ToByte(hexaColor.Substring(5, 2), 16)
                    //Convert.ToByte(hexaColor.Substring(7, 2), 16)
                )
            );
        }

        //private ObservableCollection<FactUIModel> factItems;

        private List<Rule> rules;
        private List<Item> items;
        private Dictionary<string, Item> itemsDictionary;
        private Dictionary<string, factItem> modelItems;
        private InferenceEngine inferenceEngine;

        public MainActivity()
        {
            InitializeComponent();
            LoadData();
            inferenceEngine = new InferenceEngine(rules, itemsDictionary);
        }

        private void LoadData()
        {
            List<TmpRule> tmpRules;
            string jsonfile; 
            string json;
            rules = new List<Rule>();

            #region read and create the item code definition

            jsonfile = "Assets/rulecode.json";
            using (StreamReader objReader = new StreamReader(jsonfile))
            {
                json = objReader.ReadToEnd();
            }
            items = JsonConvert.DeserializeObject<List<Item>>(json);
            modelItems = new Dictionary<string, factItem>();
            itemsDictionary = new Dictionary<string, Item>();
            foreach (Item item in items)
            {
                item.CF = 0;
                if (!item.Question.Equals(""))
                {
                    factItem f = new factItem(item.Key, item.Description, item.Question);
                    f.CF = 0;
                    modelItems.Add(item.Key, f);
                }
                itemsDictionary.Add(item.Key, item);
                //MessageBox.Show(item.Key + "," + itemsDictionary[item.Key].Key);
            }
            
            #endregion
            #region read rule from json

            jsonfile = "Assets/rules.json";
            using (StreamReader objReader = new StreamReader(jsonfile))
            {
                json = objReader.ReadToEnd();
            }
            tmpRules = JsonConvert.DeserializeObject<List<TmpRule>>(json);

            foreach (TmpRule tmp in tmpRules)
            {
                string[] splitter = new string[] { "(", ")", ";" };
                string[] splitted = tmp.IF.Split(splitter, StringSplitOptions.RemoveEmptyEntries);

                Rule r = new Rule();
                r.Key = tmp.Key;
                //it's used for really special case....
                if (splitted.Contains("OR") && splitted.Contains("AND"))
                {
                    for (int i = 2; i < splitted.Count() - 1; i++)
                    {
                        r.OR.Add(splitted[i]);
                    }
                    r.AND.Add(splitted.Last());
                }
                else
                {
                    for (int i = 0; i < splitted.Count(); i++)
                    {
                        if (splitted.Contains("OR") && !splitted[i].Equals("OR")) r.OR.Add(splitted[i]);
                        else if (!splitted[i].Equals("AND") && !splitted[i].Equals("OR")) r.AND.Add(splitted[i]); // all singleton is placed to AND
                    }
                }
                r.THEN = tmp.THEN;
                r.CF = tmp.CF;
                rules.Add(r);
            }
            #endregion

            CreateCat();
            int iter = 0;
            foreach (var item in modelItems){
                Visibility v;
                //if (categor.BEF.Contains(item.Key) && categor.BVD.Contains(item.Key)
                //    && categor.BVD2.Contains(item.Key) && categor.PMK.Contains(item.Key))
                //{
                //    v = System.Windows.Visibility.Visible;
                //}
                if (iter < 6)
                {
                    v = System.Windows.Visibility.Visible;
                }
                else
                {
                    v = System.Windows.Visibility.Collapsed;
                }
                iter++;
                addRes(item.Key, item.Value.Question, v);
            }
            
        }

        private Category categor;
        private void CreateCat()
        {
            categor = new Category();
        }

        private void ShowQuestion()
        {
            int maxQuestionShowing = 6;
            int i = 0;
            Item x = itemsDictionary[inferenceEngine.TheGreatestOfAll()];
            if (x.CF > InferenceEngine.CF_TREESHOLD)
            {
                //niatnya ganti halaman nunjukin konklusi dan nilai cf nya
                MessageBoxResult res = MessageBox.Show("Do you want to see the result right now or not?", "Conclusion criteria are met", MessageBoxButton.OKCancel);
                if (res == MessageBoxResult.OK)
                {
                    PhoneApplicationService.Current.State["item"] = x;
                    this.NavigationService.Navigate(new Uri("/Conclu.xaml", UriKind.RelativeOrAbsolute));
                }
            }

            foreach (StackPanel factUI in this.content.Children)
            {
                if (i < maxQuestionShowing)
                {
                    if (!inferenceEngine.IsHere(factUI.Name))
                    {
                        factUI.Visibility = System.Windows.Visibility.Visible;
                        if (i == 0)
                        {
                            scrollViewer.ScrollToVerticalOffset(0);
                        }
                        i++;
                    }
                }
                //if (categor.Disease[x.Key].Contains(factUI.Name))
                //{
                //    foreach (var item in categor.Disease[x.Key])
                //    {
                //        if (!inferenceEngine.IsHere(factUI.Name))
                //        {
                //            if (item.Equals(factUI.Name))
                //            {
                //                factUI.Visibility = System.Windows.Visibility.Visible;
                //                i++;
                //            }

                //        }

                //    }
                //}
            }
            if (i == 0)
            {
                PhoneApplicationService.Current.State["item"] = x;
                this.NavigationService.Navigate(new Uri("/Conclu.xaml", UriKind.RelativeOrAbsolute));
            }
        }  

        //private void factItems_CollectionChanged(object sender, System.Collections.Specialized.NotifyCollectionChangedEventArgs e)
        //{
        //    var collection = sender as ObservableCollection<FactUIModel>;
        //    var item = collection.LastOrDefault();
        //    if (item != null)
        //    {
        //        this.listBox.BringIntoView(item);
        //    }
        //}

        #region UI
        private void addRes(string name, string questext, Visibility v)
        {
            StackPanel wrapper = new StackPanel();
            wrapper.Margin = new Thickness(12, 6, 12, 6);
            wrapper.Name = name;//##############################
            wrapper.Orientation = System.Windows.Controls.Orientation.Vertical;
            wrapper.Background = new SolidColorBrush(Colors.White);
            wrapper.HorizontalAlignment = System.Windows.HorizontalAlignment.Stretch;
            wrapper.VerticalAlignment = System.Windows.VerticalAlignment.Top;
            wrapper.Visibility = v; //######
            
            TextBlock ques = new TextBlock();
            ques.FontSize = 28;
            ques.Margin = new Thickness(12);
            ques.Text = questext; //#######################
            ques.TextWrapping = TextWrapping.Wrap;

            Slider slid = new Slider();
            slid.Style = App.Current.Resources["SliderFact"] as Style;
            slid.Value = 0; //#########################
            slid.ValueChanged += Slider_ValueChanged;

            Grid grid = new Grid();
            grid.Margin = new Thickness(12, -28, 12, 6);
            TextBlock y = new TextBlock();
            y.Name = "ya";
            y.Text = "YA";
            y.HorizontalAlignment = System.Windows.HorizontalAlignment.Right;
            y.Tap += TapInputCF;
            TextBlock mboh = new TextBlock();
            mboh.Name = "mboh";
            mboh.Text = "TIDAK TAHU";
            mboh.HorizontalAlignment = System.Windows.HorizontalAlignment.Center;
            mboh.Tap += TapInputCF;
            TextBlock n = new TextBlock();
            n.Name = "tidak";
            n.Text = "TIDAK";
            n.HorizontalAlignment = System.Windows.HorizontalAlignment.Left;
            n.Tap += TapInputCF;
            grid.Children.Add(y);
            grid.Children.Add(mboh);
            grid.Children.Add(n);

            Border spliter = new Border();
            spliter.BorderBrush = GetColorFromHexa("#E3E3E3");
            spliter.BorderThickness = new Thickness(2);
            spliter.SetValue(Canvas.ZIndexProperty, 2);

            TextBlock persen = new TextBlock();
            persen.Name = "persen";
            persen.Margin = new Thickness(12, 0, 12, 12);
            persen.Text = "Kepastian: 0 %";//#######################
            persen.HorizontalAlignment = System.Windows.HorizontalAlignment.Right;

            wrapper.Children.Add(ques);
            wrapper.Children.Add(slid);
            wrapper.Children.Add(grid);
            wrapper.Children.Add(spliter);
            wrapper.Children.Add(persen);

            this.content.Children.Add(wrapper);
            //this.scrollViewer.Content = wrapper;

        }

        #endregion
        private void proceedBtn_Click(object sender, EventArgs e)
        {
            foreach (StackPanel factUI in this.content.Children)
            {
                if (factUI.Visibility == System.Windows.Visibility.Visible)
                {
                    Slider slid = factUI.FindChildByType<Slider>();
                    Item x = new Item()
                    {
                        Key = factUI.Name,
                        CF = Math.Round(slid.Value / 2, 2),
                        Description = itemsDictionary[factUI.Name].Description,
                        Question = itemsDictionary[factUI.Name].Question
                    };
                    inferenceEngine.AddWorkingMemory(x);
                    factUI.Visibility = System.Windows.Visibility.Collapsed;
                }
            }
            inferenceEngine.Run();
            ShowQuestion();
            //foreach (var item in itemsDictionary)
            //{
            //    MessageBox.Show(item.Key + ", " + item.Value.Key + ", " + item.Value.CF);
            //}
            //MessageBox.Show(itemsDictionary["Z001"].CF.ToString() + ", " +
            //    itemsDictionary["Z002"].CF.ToString() + ", " + itemsDictionary["Z003"].CF.ToString() +
            //    ", " + itemsDictionary["Z004"].CF.ToString());
        }

        private void TapInputCF(object sender, System.Windows.Input.GestureEventArgs e)
        {
            TextBlock t = sender as TextBlock;
            Grid parent = t.Parent as Grid;
            StackPanel parentOfParent = parent.Parent as StackPanel;

            Slider x = parentOfParent.FindChildByType<Slider>();
            x.Value = (t.Name.Equals("mboh")) ? 0 : (t.Name.Equals("ya") ? x.Maximum : x.Minimum);
        }

        private void Slider_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            Slider x = sender as Slider;
            StackPanel parent = x.Parent as StackPanel;
            List<TextBlock> xx = new List<TextBlock>();
            xx.AddRange(parent.ChildrenOfType<TextBlock>());
                
            foreach(TextBlock t in xx)
            {
                if (t.Name.Equals("persen"))
                {
                    double val = e.NewValue / 2;
                    t.Text = "Kepastian: " + (Math.Round(val, 2) * 100).ToString() + " %";
                    if (Math.Round(val, 2) * 100 > 0) t.Foreground = new SolidColorBrush(Colors.Green);
                    else if (Math.Round(val, 2) * 100 < 0) t.Foreground = new SolidColorBrush(Colors.Red);
                    else t.Foreground = new SolidColorBrush(Colors.Black);
                }
            }
                
        }

        private void cancelBtn_Click(object sender, EventArgs e)
        {
            MessageBox.Show("We are still working on the functionality of resetting the working memory");
        }

    }


}