﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ternak_Ternik
{
    class Item
    {
        public string Key { get; set; }
        public string Description { get; set; }
        public string Question { get; set; }
        public double CF { get; set; }
    }
}
