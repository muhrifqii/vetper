﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ternak_Ternik
{
    public class TmpRule
    {
        public string Key { get; set; }
        public string Rule { get; set; }
        public string IF { get; set; }
        public string THEN { get; set; }
        public double CF { get; set; }
    }
}
